﻿using ProductApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProductApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InsertEmployeePage : ContentPage
    {
        public InsertEmployeePage()
        {
            InitializeComponent();
            btnSave.Clicked += BtnSave_Clicked;
        }

        private async void BtnSave_Clicked(object sender, EventArgs e)
        {
            var newEmp = new Employee
            {
                EmpName = txtEmpName.Text,
                Department = txtDepartment.Text,
                Qualification = txtQualification.Text
            };

            App.DbUtils.SaveEmployee(newEmp);
            Toast.ShortMessage("Data Employee Berhasil Ditambah !");
            await Navigation.PopAsync();
        }
    }
}