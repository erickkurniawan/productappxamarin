﻿using ProductApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProductApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DaftarEmployeePage : ContentPage
    {
        private List<Employee> dataEmployee;
        public DaftarEmployeePage()
        {
            InitializeComponent();
            btnCreate.Clicked += BtnCreate_Clicked;
            listEmployee.ItemTapped += ListEmployee_ItemTapped;
            //btnSearch.Clicked += BtnSearch_Clicked;

            txtSearch.TextChanged += TxtSearch_TextChanged;
        }

        private void TxtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            var results = from emp in dataEmployee
                          where emp.EmpName.ToLower().Contains(txtSearch.Text.ToLower())
                          select emp;

            listEmployee.ItemsSource = results;
        }

        private void BtnSearch_Clicked(object sender, EventArgs e)
        {
            var results = from emp in dataEmployee
                          where emp.EmpName.ToLower().Contains(txtSearch.Text.ToLower())
                          select emp;

            listEmployee.ItemsSource = results;
        }

        private async void ListEmployee_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var currEmp = (Employee)e.Item;
            var formDetail = new DetailEmployeePage();
            formDetail.BindingContext = currEmp;
            await Navigation.PushAsync(formDetail);
        }

        private async void BtnCreate_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new InsertEmployeePage());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            dataEmployee = App.DbUtils.GetAllEmployee().ToList();
            listEmployee.ItemsSource = dataEmployee;
        }


    }
}