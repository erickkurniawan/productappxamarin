﻿using ProductApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProductApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailEmployeePage : ContentPage
    {
        public DetailEmployeePage()
        {
            InitializeComponent();
            btnEdit.Clicked += BtnEdit_Clicked;
            btnDelete.Clicked += BtnDelete_Clicked;

        }

        private async void BtnDelete_Clicked(object sender, EventArgs e)
        {
            var deleteEmp = new Employee
            {
                EmpId = Convert.ToInt64(txtId.Text)
            };

            var result = await DisplayAlert("Konfirmasi", "Delete data ?", "Yes", "No");
            if (result)
            {
                App.DbUtils.DeleteEmployee(deleteEmp);
                Toast.LongMessage("Delete data berhasil");
                await Navigation.PopAsync();
            }
        }

        private async void BtnEdit_Clicked(object sender, EventArgs e)
        {
            var editEmp = new Employee
            {
                EmpId = Convert.ToInt64(txtId.Text),
                EmpName = txtEmpName.Text,
                Department = txtDepartment.Text,
                Qualification = txtQualification.Text
            };

            App.DbUtils.UpdateEmployee(editEmp);
            Toast.LongMessage("Update Emp Berhasil..");
            await Navigation.PopAsync();
        }
    }
}