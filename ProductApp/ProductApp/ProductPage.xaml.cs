﻿using ProductApp.Models;
using ProductApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProductApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductPage : ContentPage
    {
        private ProductServices productServices;
        public ProductPage()
        {
            InitializeComponent();
            productServices = new ProductServices();
            btnCreate.Clicked += BtnCreate_Clicked;
            productList.Refreshing += ProductList_Refreshing;
            productList.ItemTapped += ProductList_ItemTapped;
            btnGetAppCurrent.Clicked += BtnGetAppCurrent_Clicked;
        }

        private void BtnGetAppCurrent_Clicked(object sender, EventArgs e)
        {
            string nama = Application.Current.Properties["username"].ToString();
            DisplayAlert("Keterangan", "Username : " + nama, "OK");
        }

        private async void ProductList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var product = (Product)e.Item;
            ProductDetail formDetail = new ProductDetail();
            formDetail.BindingContext = product;
            await Navigation.PushAsync(formDetail);
        }

        private async void ProductList_Refreshing(object sender, EventArgs e)
        {
            productList.ItemsSource = await productServices.GetAll();
            productList.IsRefreshing = false;
        }

        private async void BtnCreate_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CreateProductPage());
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            productList.ItemsSource = await productServices.GetAll();
        }




    }
}