﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProductApp
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            btnToast.Clicked += BtnToast_Clicked;
            btnSetAppCurrent.Clicked += BtnSetAppCurrent_Clicked;
		}

        private async void BtnSetAppCurrent_Clicked(object sender, EventArgs e)
        {
            Application.Current.Properties["username"] = "Erick";
            await Navigation.PushAsync(new ProductPage());
        }

        private void BtnToast_Clicked(object sender, EventArgs e)
        {
            Toast.ShortMessage("Hello Dependency Services");
        }
    }
}
