﻿using ProductApp.Models;
using ProductApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProductApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateProductPage : ContentPage
    {
        private ProductServices productServices;
        public CreateProductPage()
        {
            InitializeComponent();
            productServices = new ProductServices();
            btnCreate.Clicked += BtnCreate_Clicked;
        }

        private async void BtnCreate_Clicked(object sender, EventArgs e)
        {
            try
            {
                var newProduct = new Product
                {
                    ProductName = txtProductName.Text,
                    Quantity = Convert.ToInt32(txtQuantity.Text),
                    Price = Convert.ToDecimal(txtPrice.Text)
                };
                var result = await productServices.Create(newProduct);
                if (result)
                {
                    await DisplayAlert("Konfirmasi", "Data Produk " + txtProductName.Text + " berhasil ditambahkan", "OK");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Konfirmasi", "Data Produk gagal ditambah", "OK");
                }
                
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
            }
        }
    }
}