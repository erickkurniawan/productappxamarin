﻿using ProductApp.Models;
using ProductApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProductApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductDetail : ContentPage
    {
        private ProductServices productServices;
        public ProductDetail()
        {
            InitializeComponent();
            btnUpdate.Clicked += BtnUpdate_Clicked;
            productServices = new ProductServices();
        }

        private async void BtnUpdate_Clicked(object sender, EventArgs e)
        {
            try
            {
                var updateData = new Product
                {
                    ProductName = txtProductName.Text,
                    Quantity = Convert.ToInt32(txtQuantity.Text),
                    Price = Convert.ToDecimal(txtPrice.Text)
                };
                var result = await productServices.Update(txtProductID.Text, updateData);
                if (result)
                {
                    await DisplayAlert("Konfirmasi", "Data Produk " + txtProductName.Text + " berhasil diupdate", "OK");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Konfirmasi", "Data Produk gagal diupdate", "OK");
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}