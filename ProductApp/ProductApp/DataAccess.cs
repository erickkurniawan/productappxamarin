﻿using Plugin.NetStandardStorage.Abstractions.Interfaces;
using Plugin.NetStandardStorage.Implementations;
using ProductApp.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProductApp
{
    public class DataAccess
    {
        private SQLiteConnection database;

        public DataAccess()
        {
            database = GetConnection();
            database.CreateTable<Employee>();
        }

        public SQLiteConnection GetConnection()
        {
            SQLiteConnection sqliteConnection;
            var sqliteFilename = "MyDb.db3";
            IFolder folder = new FileSystem().LocalStorage;
            string path = Path.Combine(folder.FullPath, sqliteFilename);
            sqliteConnection = new SQLiteConnection(path);
            return sqliteConnection;
        }

        public IEnumerable<Employee> GetAllEmployee()
        {
            return database.Query<Employee>("select * from Employee order by EmpName");
        }

        public int SaveEmployee(Employee employee)
        {
            return database.Insert(employee);
        }

        public int DeleteEmployee(Employee employee)
        {
            return database.Delete(employee);
        }

        public int UpdateEmployee(Employee employee)
        {
            return database.Update(employee);
        }
    }
}
