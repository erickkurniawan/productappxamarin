﻿using Newtonsoft.Json;
using ProductApp.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Services
{
    public class ProductServices
    {
        private HttpClient _client;
        public ProductServices()
        {
            _client = new HttpClient();
        }

        public async Task<List<Product>> GetAll()
        {
            List<Product> listProduct = null;
            var uri = new Uri("http://kurniaservices.azurewebsites.net/api/Product");
            try
            {
                var response = await _client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    listProduct = JsonConvert.DeserializeObject<List<Product>>(content);
                }
                return listProduct;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<Product> GetById(string id)
        {
            Product product = null;
            var uri = new Uri($"http://kurniaservices.azurewebsites.net/api/Product/{id}");
            try
            {
                var response = await _client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    product = JsonConvert.DeserializeObject<Product>(content);
                }
                return product;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> Create(Product product)
        {
            bool isSuccess = false;
            var uri = new Uri("http://kurniaservices.azurewebsites.net/api/Product");
            try
            {
                var jsonData = JsonConvert.SerializeObject(product);
                var content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                var response = await _client.PostAsync(uri, content);

                if (response.IsSuccessStatusCode)
                {
                    isSuccess = true;
                }
                return isSuccess;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> Update(string id,Product product)
        {
           
            var uri = new Uri($"http://kurniaservices.azurewebsites.net/api/Product/{id}");
            try
            {
                var jsonData = JsonConvert.SerializeObject(product);
                var content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                var response = await _client.PutAsync(uri, content);

                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> Delete(string id)
        {
            var uri = new Uri($"http://kurniaservices.azurewebsites.net/api/Product/{id}");
            try
            {
                var response = await _client.DeleteAsync(uri);
                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
